import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./tab.scss";

export default class Tab extends Component {
  render() {
    const { imgs, labels } = this.props;

    const tabs = labels.map((label, idx) => {
      let path = "";
      if (idx === 0) {
        path = "/main/cities/";
      } else if (idx === 1) {
        path = "/main/orders";
      } else path = "/main/profile";
      return (
        <Link to={path} key={idx} className="control-side__tab">
          <img src={imgs[idx]} width="34" height="34" alt=""/>
          <span>{label}</span>
        </Link>
      );
    });
    return <>{tabs}</>;
  }
}
