import React, { Component } from "react";
import bikeMan from "../../img/bikeMan.png"
import "./illustration-lock.scss"

export default class IllustrationLock extends Component {
  render() {
    return <img className="main-illustration" src={bikeMan} alt=""/>;
  }
}
