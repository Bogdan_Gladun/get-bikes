import React, { Component } from "react";
import Input from "../Input/Input";
import CitiesList from "../CitiesList/CitiesList";
import Centers from "../Centers/Centers";
import CmsApiService from "../../services/CmsApiService";

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import "../ControlSide/control-side.scss";

export default class Cities extends Component {
  render() {
    const { cityCenters, chooseTown, loading, cityArray, chooseCenter, townId } = this.props;
    const cityList = loading ? (
      <span>Not done...</span>
    ) : (
      <CitiesList items={cityArray} chooseTown={chooseTown} townId={townId}/>
    );
    return (
      <div className="control-side__main-wrapper">
        <div className="control-side__seacrh-wrapper">
          <Input
            label="Type your city"
            type="input"
            placeholder="Let's search your city"
            name="searchField"
          />
        </div>
        {cityList}
        <Centers cityCenters={cityCenters} chooseCenter={chooseCenter} />
      </div>
    );
  }
}
