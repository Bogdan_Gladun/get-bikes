import React, { Component } from "react";
import AuthSide from "../AuthSide/AuthSide";
import IllustrationLock from "../IllustrationLock/IllustrationLock";
import "./auth-page.css";

export default class AuthPage extends Component {
  render() {
    const {
      email,
      password,
      fullName,
      signedUp,
      errMsg,
      error,
      handleInput,
      signedUpSwitcher,
      authorize
    } = this.props;
    return (
      <div className="auth-page__flex-wrapper">
        <AuthSide
          history={this.props.history}
          email={email}
          password={password}
          fullName={fullName}
          signedUp={signedUp}
          errMsg={errMsg}
          error={error}
          handleInput={handleInput}
          signedUpSwitcher={signedUpSwitcher}
          authorize={authorize}
        />
        <IllustrationLock />
      </div>
    );
  }
}
