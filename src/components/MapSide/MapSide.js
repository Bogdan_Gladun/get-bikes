import React, { Component } from "react";
import Map from "../Map/Map";

export default class MainPage extends Component {
  render() {
    const { townId, updateCity } = this.props;
    console.log(`Map ${townId}`);
    return <Map townId={townId} updateCity={updateCity} />;
  }
}
