import React, { Component } from "react";
import ControlSide from "../ControlSide/ControlSide";
import MapSide from "../MapSide/MapSide";
import MapService from "../../services/MapService";
import NavigationBtn from "../NavigationBtn/NavigationBtn";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import "./main-page.scss";
import CmsApiService from "../../services/CmsApiService";

export default class MainPage extends Component {
  cmsApiService = new CmsApiService();
  mapService = new MapService();
  state = {
    townId: null,
    city: null,
    location: null,
    cityCenters: [],
    cities: [],
    loading: true,
    whichDay: null,
    startTime: null,
    bikesAmounToRent: null,
    howLongToRent: null,
    endTime: null,
    amount: null,
    endTimestamp: null,
    startTimestamp: null,
    centerId: null,
    bookingsHistory: [],
    pickedUp: false,
    errMsg: ""
  };

  handleMountCities = () => {
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService._getCities,
        method: "GET",
        token: true
      })
      .then(({ Items }) => {
        this.setState({
          cities: Items,
          loading: false
        });
      });
  };

  getSupposedCost = state => {
    const { whichDay, startTime, endTime, bikesAmounToRent, centerId } = state;
    console.log(state);
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService.getSupposedCostUrl(
          whichDay,
          startTime,
          endTime,
          bikesAmounToRent,
          centerId
        ),
        token: true
      })
      .then(body => {
        this.setState({
          amount: body.bookingCostGeneral,
          endTimestamp: body.endTimestamp,
          startTimestamp: body.startTimestamp
        });
      })
      .catch(err => {
        alert(err);
        console.log(err);
      });
  };

  getEndTime = (startTime, rentTime) => {
    if (!startTime || !rentTime) {
      return;
    }
    let tempTime = parseInt(startTime.slice(0, 3));
    console.log(`${tempTime + parseInt(rentTime)}:${startTime.slice(3)}`);
    this.setState({
      endTime: `${tempTime + parseInt(rentTime)}:${startTime.slice(3)}`
    });
  };

  handleInput = ({ target }) => {
    this.setState({
      [target.name]: target.value,
      amount: null
    });
  };

  chooseTown = id => {
    this.setState({
      townId: id
    });
  };

  chooseCenter = id => {
    this.setState({
      centerId: id
    });
  };

  updateCity = map => {
    this.cmsApiService
      .makingRequest({
        url: `${this.cmsApiService._getCitie}${this.state.townId}`,
        method: "GET",
        token: true
      })
      .then(body => {
        this.setState(
          {
            city: body.city,
            location: body.location
          },
          () => {
            map.setView([this.state.location.lat, this.state.location.lon]);
            this.updateCenters(map);
          }
        );
      });
  };

  updateCenters = map => {
    this.cmsApiService
      .makingRequest({
        url: `${this.cmsApiService._getCenters}${this.state.city}`,
        token: true
      })
      .then(body => {
        this.setState(
          {
            cityCenters: body.Items
          },
          () => {
            this.handleBindMarker(this.state, map);
          }
        );
      })
      .catch(err => console.log(err));
  };

  getBookingHistory = (id) => {
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService.getBookingHistoryUrl(id),
        token: true
      })
      .then(body => {
        this.setState({
          bookingsHistory: body
        });
      })
      .catch(err => alert(err))
  };

  pickUpBike = (bookingId, userId) => {
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService.getPickUpBikeUrl(bookingId),
        token: true,
        method: "POST"
      })
      .then(body => this.getBookingHistory(userId))
      .catch(err => alert(err))
  };

  dropOffBike = (bookingId, userId) => {
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService.getdropOffBikeUrl(bookingId),
        token: true,
        method: "POST"
      })
      .then(body => this.getBookingHistory(userId))
      .catch(err => alert(err))
  };

  handleBindMarker = ({ cityCenters }, map) => {
    const numberOfCenters = new Array(cityCenters.length);
    for (let i = 0; i < numberOfCenters.length; i++) {
      numberOfCenters[i] = new this.mapService.LeafIcon();
      this.mapService.bindMarker(
        map,
        cityCenters[i].location.lat,
        cityCenters[i].location.lon,
        numberOfCenters[i],
        this.renderdHTML(cityCenters[i])
      );
    }
  };

  findSuitableId = (cityName, cityArray) => {
    for (let i = 0; i < cityArray.length; i++) {
      if (cityName === cityArray[i].city) {
        this.chooseTown(cityArray[i].id);
      }
    }
  };

  createBooking = body => {
    this.cmsApiService
      .makingRequest({
        url: `${this.cmsApiService._apiBase}/bookings/${this.state.city}`,
        body: body,
        token: true,
        method: "POST"
      })
      .then(body => {
        console.log(body);
        this.props.history.push("/main/cities/");
      });
  };

  navigate = () => {
    navigator.geolocation.getCurrentPosition(position => {
      if (position) {
        this.cmsApiService.makingRequest({
          url: this.cmsApiService.getNominatimUrl(position.coords.latitude, position.coords.longitude)
        })
          .then(({ address }) =>
            this.findSuitableId(address.city, this.state.cities)
          )
          .catch(err => alert(err))
      }
    });
  };

  renderdHTML = cityCenters => {
    return `<span>Address: ${cityCenters.location.address}</span>
            <p>Monday: ${cityCenters.availableTime.monday.openTime}-${
      cityCenters.availableTime.monday.closeTime
    }</p>
            <p>Tuesday: ${cityCenters.availableTime.tuesday.openTime}-${
      cityCenters.availableTime.tuesday.closeTime
    }</p>`;
  };
  render() {
    const { userId, fullName, email, updateUser} = this.props;
    const {
      townId,
      cityCenters,
      loading,
      cities,
      startTime,
      howLongToRent,
      amount
    } = this.state;
    return (
      <div className="main-page__wrapper">
        <ControlSide
          townId={townId}
          chooseTown={this.chooseTown}
          cityCenters={cityCenters[0] ? cityCenters : null}
          userId={userId}
          fullName={fullName}
          email={email}
          updateUser={updateUser}
          loading={loading}
          cities={cities}
          handleMountCities={this.handleMountCities}
          handleInput={this.handleInput}
          getEndTime={() => this.getEndTime(startTime, howLongToRent)}
          getSupposedCost={() => {
            this.getSupposedCost(this.state);
          }}
          amount={amount}
          chooseCenter={this.chooseCenter}
          createBooking={this.createBooking}
          mainState={this.state}
          getBookingHistory={this.getBookingHistory}
          pickUpBike={this.pickUpBike}
          dropOffBike={this.dropOffBike}
        />
        <MapSide townId={townId} updateCity={this.updateCity} />
        <NavigationBtn navigate={this.navigate} />
      </div>
    );
  }
}
