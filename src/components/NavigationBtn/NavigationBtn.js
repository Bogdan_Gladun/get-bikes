import React, { Component } from "react";

import "./navigation-btn.scss";

export default class NavigationBtn extends Component {
  render() {
    return (
      <button
        className="navigation-btn"
        onClick={() => {
          this.props.navigate();
        }}
      >
        Navigate
      </button>
    );
  }
}
