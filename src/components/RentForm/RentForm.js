import React, { Component } from "react";
import CmsApiService from "../../services/CmsApiService";
import "./rent-form.scss";

export default class RentForm extends Component {
  cmsApiService = new CmsApiService();

  render() {
    const { handleInput, getSupposedCost, getEndTime, amount } = this.props;
    const message = amount ? (
      <p>General cost is {(amount * 0.01).toFixed(2)}eur</p>
    ) : null;
    return (
      <div>
        <form className="rent-form">
          <p className="rent-form__question">When do you want to start</p>
          <div className="rent-form__fields-wrapper">
            <div className="rent-form__start-time">
              <label className="rent-form__label" htmlFor="dayId">
                Day
              </label>
              <input
                className="form-rent__input"
                id="dayId"
                type="date"
                name="whichDay"
                min="2019-02-11"
                max="2019-12-31"
                onChange={handleInput}
              />
            </div>
            <div>
              <label className="rent-form__label" htmlFor="timeId">
                Time
              </label>
              <input
                className="form-rent__input"
                id="timeId"
                type="time"
                name="startTime"
                min="9:00"
                max="16:00"
                onChange={handleInput}
              />
            </div>
          </div>
          <div className="">
            <p className="rent-form__question">How long do you want to ride?</p>
            <label className="rent-form__label" htmlFor="rentHoursId">
              Hours
            </label>
            <input
              className="form-rent__input"
              id="rentHoursId"
              type="number"
              name="howLongToRent"
              min="1"
              max="3"
              onChange={handleInput}
            />
            <p className="rent-form__question">
              How many bikes do you want to rent?
            </p>
            <label className="rent-form__label" htmlFor="amountBikesId">
              Select{" "}
            </label>
            <input
              className="form-rent__input"
              id="amountBikesId"
              type="number"
              name="bikesAmounToRent"
              min="1"
              max="50"
              onChange={handleInput}
            />
          </div>
        </form>
        {message}
        <button
          className="button"
          onClick={getSupposedCost}
          onMouseDown={getEndTime}
        >
          Get supposed cost
        </button>
      </div>
    );
  }
}
