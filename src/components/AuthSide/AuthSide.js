import React, { Component } from "react";
import AuthForm from "../AuthForm/AuthForm";
import "./auth-side.scss";
import logo from "../../img/logo.png";

export default class AuthSide extends Component {
  render() {
    const {
      email,
      password,
      fullName,
      signedUp,
      errMsg,
      error,
      handleInput,
      signedUpSwitcher,
      authorize
    } = this.props;
    return (
      <div className="auth-side__wrapper">
        <img className="auth-side__img" src={logo} />
        <AuthForm
          history={this.props.history}
          email={email}
          password={password}
          fullName={fullName}
          signedUp={signedUp}
          errMsg={errMsg}
          error={error}
          handleInput={handleInput}
          signedUpSwitcher={signedUpSwitcher}
          authorize={authorize}
        />
      </div>
    );
  }
}
