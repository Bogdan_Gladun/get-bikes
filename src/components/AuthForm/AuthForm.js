import React, { Component } from "react";
import Button from "../Button/Button";
import CmsApiService from "../../services/CmsApiService";
import Input from "../Input/Input";

import "./auth-form.scss";

export default class AuthForm extends Component {
  cmsApiService = new CmsApiService();

  render() {
    const {
      email,
      password,
      fullName,
      signedUp,
      errMsg,
      error,
      handleInput,
      signedUpSwitcher,
      authorize
    } = this.props;
    console.log(errMsg);
    const ErrorMsg = error ? <span>{errMsg}</span> : null;

    const fullNameInput = signedUp ? (
      <Input
        label="FullName"
        type="input"
        placeholder="Fullname"
        name="fullName"
        value={fullName}
        onChange={e => handleInput(e)}
      />
    ) : null;

    const signInBtn = signedUp ? null : (
      <Button
        label={"Sign in"}
        logIn={() =>
          authorize(this.cmsApiService._signInUrl, { email, password }, "POST")
        }
      />
    );

    return (
      <form className="auth-form">
        <div className="auth-form__wrapper">
          <Input
            label="Email"
            type="input"
            placeholder="Email"
            name="email"
            value={email}
            onChange={e => handleInput(e)}
          />

          <Input
            label="Password"
            type="password"
            placeholder="Password"
            name="password"
            value={password}
            onChange={e => handleInput(e)}
          />
          {fullNameInput}
        </div>

        {ErrorMsg}
        {signInBtn}
        <span className="auth-form__separator">OR</span>
        <Button
          label={"Sign up"}
          logIn={() => {
            return signedUp
              ? authorize(
                  this.cmsApiService._signUpUrl,
                  { email, password, fullName },
                  "POST"
                )
              : signedUpSwitcher();
          }}
        />
      </form>
    );
  }
}
