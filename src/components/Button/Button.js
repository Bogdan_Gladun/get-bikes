import React, { Component } from "react";
import "./button.scss";

export default class Button extends Component {
  render() {
    const { label, logIn } = this.props;
    return (
      <button className="button" type="button" onClick={logIn}>
        {label}
      </button>
    );
  }
}
