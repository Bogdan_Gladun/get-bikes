import React, { Component } from "react";
import "leaflet/dist/leaflet.css";
import MapService from "../../services/MapService";
import CmsApiService from "../../services/CmsApiService";


import "./map.scss";

export default class Map extends Component {
  mapService = new MapService();
  cmsApiService = new CmsApiService();

  componentDidMount() {
    this.map = this.mapService.initMap(this.map, 48.857012, 2.353066);
    this.mapService.initLayer(this.map);
  }

  componentDidUpdate(prevProps) {
    if (this.props.townId !== prevProps.townId) {
       this.props.updateCity(this.map);
    }
  }
  

  render() {
    
    return <div className="map" id="map" />;
    
  }
}


