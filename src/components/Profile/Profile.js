import React, { Component } from "react";
import Input from "../Input/Input";
import "./profile.scss";
import "../Button/button.scss";

export default class Profile extends Component {
  state = {
    userId: "",
    fullName: "",
    email: "",
    modify: false
  };
  componentDidMount() {
    const userData = localStorage.getItem("newUser")
      ? JSON.parse(localStorage.getItem("newUser"))
      : JSON.parse(localStorage.getItem("userData")).Item;
    this.setState({
      userId: userData.id,
      fullName: userData.fullName,
      email: userData.email
    });
  }
  handleModify = () => {
    this.setState({
      modify: true
    });
  };
  handleInput = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  };
  render() {
    const { updateUser } = this.props;
    const { modify } = this.state;
    console.log(this.state);
    return (
      <div className="profile">
        <Input
          label="FullName"
          type="input"
          placeholder="Fullname"
          name="fullName"
          value={this.state.fullName}
          disabled={modify ? false : true}
          onChange={this.handleInput}
        />
        <Input
          label="Email"
          type="input"
          placeholder="Email"
          name="email"
          value={this.state.email}
          disabled={modify ? false : true}
          onChange={this.handleInput}
        />
        <div className="profile__btn-wrapper">
          <button className="button" onClick={this.handleModify}>
            Modify
          </button>
          <button
            className="button"
            onClick={() => {
              updateUser(
                { fullName: this.state.fullName, email: this.state.email },
                this.state.userId
              );
              this.setState({
                modify: false
              });
            }}
          >
            Save changes
          </button>
        </div>
      </div>
    );
  }
}
