import React, { Component } from "react";
import AuthPage from "../AuthPage/AuthPage";
import MainPage from "../MainPage/MainPage";
import CmsApiService from "../../services/CmsApiService";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";

export default class App extends Component {
  cmsApiService = new CmsApiService();

  state = {
    email: "",
    password: "",
    fullName: "",
    signedUp: false,
    error: false,
    errMsg: "",
    id: ""
  };

  authorize = (url, body, method) => {
    this.cmsApiService
      .makingRequest({ url, body, method })
      .then(body => {
        this.setState({
          id: body.Item.id,
          fullName: body.Item.fullName
        });
        localStorage.setItem("userData", JSON.stringify(body));
        this.props.history.push("/main/cities/");
      })
      .catch(err => {
        this.setState({
          errMsg: err.toString().slice(6),
          error: true
        });
      });
  };

  handleInput = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  };

  signedUpSwitcher = () => {
    this.setState(({ signedUp }) => {
      return {
        signedUp: true
      };
    });
  };

  updateUser = (body, id) => {
    this.cmsApiService
      .makingRequest({
        url: this.cmsApiService.getUpdateUserUrl(id),
        body: body,
        method: "POST",
        token: true
      })
      .then(body => localStorage.setItem("newUser", JSON.stringify(body)))
      .catch(err => alert(err));
  };

  whatToRender(item, renderElem1, renderElem2) {
    return item ? renderElem1 : renderElem2;
  }

  render() {
    const {
      email,
      password,
      fullName,
      signedUp,
      errMsg,
      error,
      id
    } = this.state;
    return (
      <Router>
        <div>
          <Route
            exact
            path="/"
            render={({ history }) => {
              console.log(this.state);
              return this.whatToRender(
                localStorage.getItem("userData"),
                <Redirect to="/main/cities/" />,
                <AuthPage
                  history={history}
                  email={email}
                  password={password}
                  fullName={fullName}
                  signedUp={signedUp}
                  errMsg={errMsg}
                  error={error}
                  handleInput={this.handleInput}
                  signedUpSwitcher={this.signedUpSwitcher}
                  authorize={this.authorize}
                />
              );
            }}
          />
          <Route
            path="/main"
            render={({ location, history }) => {
              return this.whatToRender(
                !localStorage.getItem("userData"),
                <Redirect to="/" />,
                <MainPage
                  userId={id}
                  fullName={fullName}
                  email={email}
                  updateUser={this.updateUser}
                  location={location}
                  history={history}
                />
              );
            }}
          />
        </div>
      </Router>
    );
  }
}
