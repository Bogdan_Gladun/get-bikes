import React, { Component } from "react";

import "./order-list.scss";

export default class Orders extends Component {
  userData = JSON.parse(localStorage.getItem("userData"));

  componentDidMount() {
    this.props.getBookingHistory(this.userData.Item.id);
  }

  transformData = data => {
    const index = data.indexOf("T");
    console.log(data.substr(0, index));
    console.log(data.substr(index + 1, 5));
    return {
      day: data.substr(0, index),
      time: data.substr(index + 1, 5)
    };
  };

  chooseClassByStatus = status => {
    if (status === "confirmed") {
      return "order-list__item order-list__item--confirmed";
    } else if (status === "active") {
      return "order-list__item order-list__item--active";
    } else return "order-list__item order-list__item--completed";
  };

  chooseButtonByStatus = booking => {
    if (booking.status === "confirmed") {
      return (
        <button
          onClick={() =>
            this.props.pickUpBike(booking.id, this.userData.Item.id)
          }
        >
          Active
        </button>
      );
    } else if (booking.status === "active") {
      return (
        <button
          onClick={() =>
            this.props.dropOffBike(booking.id, this.userData.Item.id)
          }
        >
          Drop
        </button>
      );
    } else return null;
  };

  render() {
    const { bookingsHistory } = this.props.mainState;

    const bookings = bookingsHistory.map((booking, idx) => {
      let statusBtn = this.chooseButtonByStatus(booking);
      const { day, time } = this.transformData(booking.startDate);
      return (
        <li className={this.chooseClassByStatus(booking.status)} key={idx}>
          <h2>To get yours {booking.bikesAmount} bike(s)</h2>
          <p>Come to {booking.city}</p>
          <p>
            at {time} on {day}
          </p>
          <p>{booking.status}</p>
          {statusBtn}
        </li>
      );
    });
    return (
      <div className="padding">
        <ul className="order-list">{bookings}</ul>
      </div>
    );
  }
}
