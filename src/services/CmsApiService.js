export default class CmsApiService {
  _apiBase = `https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0`;
  _signInUrl = `${this._apiBase}/auth/signin`;
  _signUpUrl = `${this._apiBase}/auth/signup`;
  _getCities = `${this._apiBase}/cities`;
  _getCitie = `${this._apiBase}/cities/`;
  _getCenters = `${this._apiBase}/centers/`;

  getNominatimUrl = (lat, lon) => {
    return `https://nominatim.openstreetmap.org/reverse?format=jsonv2&accept-language=en&lat=${lat}&lon=${lon}`;
  };

  getdropOffBikeUrl = id => {
    return `${this._apiBase}/bookings/${id}/dropoff`;
  };

  getBookingHistoryUrl = userId => {
    return `${this._apiBase}/bookings/user/${userId}/status/all`;
  };

  getUpdateUserUrl = id => {
    return `${this._apiBase}/users/object/${id}/update`;
  };

  getPickUpBikeUrl = id => {
    return `${this._apiBase}/bookings/${id}/pickup`;
  };

  getSupposedCostUrl = (
    whichDay,
    startTime,
    endTime,
    bikesAmounToRent,
    centerId
  ) => {
    return `${
      this._apiBase
    }/bookings/cost?start=${whichDay}+${startTime}&end=${whichDay}+${endTime}&centerId=${centerId}&bikesAmount=${bikesAmounToRent}`;
  };

  makingRequest = async ({ url, body, method, token }) => {
    console.log(url);
    const item = {
      headers: {
        "Content-type": "application/json"
      }
    };
    if (method) item.method = method;
    if (body) item.body = JSON.stringify(body);
    if (token) {
      item.headers.Authorization = `Bearer ${this.getAccesToken()}`;
    }
    const res = await fetch(`${url}`, item);
    const responseBody = await res.json();
    if (!res.ok) {
      throw new Error(responseBody.err || responseBody.message);
    }
    return responseBody;
  };

  getAccesToken = () => {
    let obj = JSON.parse(localStorage.getItem("userData"));
    for (const key in obj) {
      if (key.includes("access_token")) {
        return obj[key];
      }
    }
  };
}
