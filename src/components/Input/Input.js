import React, { Component } from "react";
import "./Input.scss";

export default class Input extends Component {
  render() {
    const {
      label,
      type,
      value,
      placeholder,
      name,
      onChange,
      disabled: isDisabled = false
    } = this.props;
    return (
      <div className="input">
        <label className="input__label">{label}</label>
        <input
          className="input__field"
          type={type}
          placeholder={placeholder}
          name={name}
          onChange={onChange}
          value={value}
          disabled={isDisabled}
        />
      </div>
    );
  }
}
