import React from "react";
import StripeCheckout from "react-stripe-checkout";

export default class TakeMoney extends React.Component {
  onToken = token => {
    const {
      amount,
      centerId,
      bikesAmounToRent,
      startTimestamp,
      endTimestamp
    } = this.props.mainState;

    if (token) {
      this.props.createBooking({
        token: token.id,
        amount: amount,
        centerId: centerId,
        bikesAmount: bikesAmounToRent,
        startTime: startTimestamp,
        endTime: endTimestamp
      });
    }

    console.log(token);
  };

  // ...

  render() {
    const { amount } = this.props;
    if (!amount) {
      return null
    } else
    return (
      // ...
      <StripeCheckout
        currency="EUR"
        amount={amount}
        token={this.onToken}
        stripeKey="pk_test_3Dp0NBdqfluanAj2D2zjj34A"
        onClick={() => console.log("STRIPE")}
      />
    );
  }
}
