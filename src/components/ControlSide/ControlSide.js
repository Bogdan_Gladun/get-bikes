import React, { Component } from "react";
import Tabs from "../Tabs/Tabs";
import Cities from "../Cities/Cities";
import Profile from "../Profile/Profile";
import TakeMoney from "../TakeMoney/TakeMoney";
import RentForm from "../RentForm/RentForm";
import Orders from "../Orders/Orders";

import "./control-side.scss";
import cities from "../../img/Cities.svg";
import order from "../../img/Order.svg";
import profile from "../../img/Profile.svg";
import CmsApiService from "../../services/CmsApiService";
import { Route, Switch } from "react-router-dom";

export default class ControlSide extends Component {
  cmsApiService = new CmsApiService();

  state = {
    tabsImgs: [cities, order, profile],
    tabsLabels: ["Cities", "Order", "Profile"]
  };

  componentDidMount = () => {
    this.props.handleMountCities();
  };

  render() {
    const {
      chooseTown,
      cityCenters,
      userId,
      fullName,
      email,
      updateUser,
      loading,
      cities,
      handleInput,
      getEndTime,
      getSupposedCost,
      amount,
      chooseCenter,
      createBooking,
      mainState,
      getBookingHistory,
      pickUpBike,
      dropOffBike,
      townId
    } = this.props;
    const { tabsImgs, tabsLabels } = this.state;

    return (
      
      <Route
        path="/main"
        render={() => {
          return (
            <div className="control-side">
              <div className="control-side__tab-wrapper">
                <Tabs imgs={tabsImgs} labels={tabsLabels} />
              </div>
              <Route
                exact
                path="/main/cities/"
                render={() => {
                  return (
                    <Cities
                      townId={townId}
                      cityCenters={cityCenters}
                      chooseTown={chooseTown}
                      loading={loading}
                      cityArray={cities}
                      chooseCenter={chooseCenter}
                    />
                  );
                }}
              />
              <Route
                path="/main/orders"
                render={() => {
                  return (
                    <Orders
                      getBookingHistory={getBookingHistory}
                      mainState={mainState}
                      pickUpBike={pickUpBike}
                      dropOffBike={dropOffBike}
                    />
                  );
                }}
              />
              <Route
                path={`/main/cities/rent`}
                render={() => {
                  return (
                    <div className="orders-wrapper">
                      <RentForm
                        amount={amount}
                        handleInput={handleInput}
                        getSupposedCost={getSupposedCost}
                        getEndTime={getEndTime}
                      />
                      <TakeMoney
                        amount={amount}
                        createBooking={createBooking}
                        mainState={mainState}
                      />
                    </div>
                  );
                }}
              />
              <Route
                path="/main/profile"
                render={() => (
                  <Profile
                    userId={userId}
                    fullName={fullName}
                    email={email}
                    updateUser={updateUser}
                  />
                )}
              />
            </div>
          );
        }}
      />
    );
  }
}
