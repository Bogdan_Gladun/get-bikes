import React from "react";
import "./city-list.scss";

const CitiesList = ({ items, chooseTown, townId }) => {
  const elements = items.map(item => {
    return (
      <li
        className={
          townId === item.id
            ? "city-list__item city-list__item--active"
            : "city-list__item"
        }
        key={item.id}
        onClick={() => {
          chooseTown(item.id);
        }}
      >
        {item.city}
      </li>
    );
  });

  return <ul className="city-list">{elements}</ul>;
};

export default CitiesList;
