import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./center-list.scss";
import "../Button/button.scss";

export default class Centers extends Component {
  render() {
    const { chooseCenter } = this.props;
    let centers = [];
    if (this.props.cityCenters) {
      centers = this.props.cityCenters.map(center => {
        return (
          <li className="center-list__item" key={center.id}>
            <div className="center-list__title-wrapper">
              <h2 className="center-list__title">Store: {center.name}</h2>
              <span className="center-list__title"> </span>
            </div>
            <div className="center-list__contetn-wrapper">
              <div className="center-list__additional-info">
                <p>
                  Adress{" "}
                  <span className="center-list__adress">
                    {center.location.address}
                  </span>
                </p>
                <p>
                  Bikes{" "}
                  <span className="center-list__bikes-count">
                    {center.bikesCount}
                  </span>{" "}
                  are available
                </p>
              </div>
              <Link
                onClick={() => chooseCenter(center.id)}
                to={`/main/cities/rent`}
                className="center-list__link button"
              >
                Get bikes
              </Link>
            </div>
          </li>
        );
      });
    }
    return <ul className="center-list">{centers}</ul>;
  }
}
