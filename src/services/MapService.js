import L from "leaflet";
import marker from "../img/pin.png";
import "leaflet/dist/leaflet.css";

export default class MapService {
  initMap = (mapName = "map", lat = 48.2, lon = 16.366667, zoom = 12) => {
    return L.map(mapName, {
      center: [lat, lon],
      zoom,
      zoomControl: true
    });
  };
  initLayer = object => {
    L.tileLayer(
      "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYm9nZGFuLWdsYWR1biIsImEiOiJjanJuc2V3dW8wdzN0NDRuenNuanh0cnR2In0.HZG6Zv5BEY5iSQZ3vB1auw",
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: "mapbox.streets",
        accessToken:
          "pk.eyJ1IjoiYm9nZGFuLWdsYWR1biIsImEiOiJjanJuc2V3dW8wdzN0NDRuenNuanh0cnR2In0.HZG6Zv5BEY5iSQZ3vB1auw"
      }
    ).addTo(object);
  };

   LeafIcon = L.Icon.extend({
    options: {
        iconUrl:      marker,
        iconSize:     [32, 32],
        iconAnchor:   [20, 15],
        popupAnchor:  [-5, -10]
    }
});
   bindMarker = (initialMap, lat = 48.817012, lon = 2.356066, iconName, htmlTemplate) => {
    L.marker([lat, lon], { icon: iconName })
    .addTo(initialMap)
    .bindPopup(htmlTemplate);
   }
}
